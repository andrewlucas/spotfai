from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from playlist import views

urlpatterns = [
    url(r'^playlist/$', views.PlaylistList.as_view()),
    url(r'^playlist/(?P<pk>[0-9]+)/$', views.PlaylistDetail.as_view()),
    url(r'^music/$', views.MusicList.as_view()),
    url(r'^music/(?P<pk>[0-9]+)/$', views.MusicDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
