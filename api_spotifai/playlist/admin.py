from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Playlist)
admin.site.register(Record)
admin.site.register(Genre)
admin.site.register(Band)
admin.site.register(Music)
