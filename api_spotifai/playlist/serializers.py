# coding=utf-8

from rest_framework import serializers
from .models import Record, Genre, Band, Music, Playlist


class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = ['name']
    def create(self, validated_data):
        return Record.objects.create(**validated_data)[0]

class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ['name']
    def create(self, validated_data):
        genre_data = validated_data.pop('genre', None)
        if genre_data:
            validated_data['genre'] = genre
        return Genre.objects.create(**genre_data)[0]

class BandSerializer(serializers.ModelSerializer):
    genre = GenreSerializer()
    record = RecordSerializer()
    class Meta:
        model = Band
        fields = ['name', 'genre', 'record']
    def create(self, validated_data):
        genre_data = validated_data.pop('genre', None)
        record_data = validated_data.pop('record', None)
        genres = Genre.objects.create(**genre_data)[0]
        records = Record.objects.create(**record_data)[0]
        validated_data['genre'] = genres
        validated_data['record'] = records
        return Band.objects.create(genre=genres,record=records, **validated_data)


class MusicSerializer(serializers.ModelSerializer):
    band = BandSerializer()
    class Meta:
        model = Music
        fields = ['name','band']
    def create(self, validated_data):
        band_data = validated_data.pop('band', None)
        if band_data:
            band = Band.objects.create(**band_data)
            validated_data['band'] = band
        return Music.objects.create(**validated_data)

class PlaylistSerializer(serializers.ModelSerializer):

    music = MusicSerializer(many=True)
    class Meta:
        model = Playlist
        fields = ['name', 'music']
    def create(self, validated_data):
        music_data = validated_data.pop('music', None)
        if music_data:
            music = Music.objects.create(**music_data)
            validated_data['music'] = music
        return Playlist.objects.create(**validated_data)
