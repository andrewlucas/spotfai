
# coding=utf-8
from __future__ import unicode_literals
from django.db import models


class Record(models.Model):
    name = models.CharField(max_length=100,blank=True)

    class Meta:
        verbose_name = 'Record'
        verbose_name_plural = 'Records'

    def __str__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(max_length=100,blank=True)

    class Meta:
        verbose_name = 'Genre'
        verbose_name_plural = 'Genres'

    def __str__(self):
        return self.name


class Band(models.Model):
    name = models.CharField(max_length=100)
    genre = models.ForeignKey(Genre, verbose_name="Genre",null=True)
    record = models.ForeignKey(Record, verbose_name='Record',null=True)

    class Meta:
        verbose_name = 'Band'
        verbose_name_plural = 'Bands'

    def __str__(self):
        return self.name


class Music(models.Model):
    name = models.CharField(max_length=100)
    band = models.ForeignKey(Band, verbose_name="Band")

    class Meta:
        verbose_name = 'Music'
        verbose_name_plural = 'Musics'

    def __str__(self):
        return self.name


class Playlist(models.Model):
    name = models.CharField(max_length=100)
    music = models.ManyToManyField(Music, verbose_name='Music')

    class Meta:
        verbose_name = 'Playlist'
        verbose_name_plural = 'Playlists'

    def __str__(self):
        return self.name
